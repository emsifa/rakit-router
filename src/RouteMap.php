<?php 

namespace Rakit\Router;

use Rakit\Router\Route;

class RouteMap {

  protected $routes = [];

  public function __construct(array $routes)
  {
    foreach($routes as $route) {
      $this->registerRoute($route);
    }
  }

  public function getRoutes()
  {
    return $this->routes;
  }

  public function registerRoute(Route $route)
  {
    $this->routes[] = $route;
  }

  public function set($key, $value)
  {
    foreach($this->routes as $route) {
      $route->set($key, $value);
    }

    return $this;
  }

  public function push($key, $value)
  {
    foreach($this->routes as $route) {
      $route->push($key, $value);
    }

    return $this;
  }

  public function allowMethod($method)
  {
    foreach($this->routes as $route) {
      $route->allowMethod($method);
    }

    return $this;
  }

  public function where($param, $condition)
  {
    foreach($this->routes as $route) {
      $route->where($param, $condition);
    }

    return $this;
  }

}