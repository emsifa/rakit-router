<?php 

namespace Rakit\Router;

use Closure;
use Rakit\Router\Route;
use Rakit\Router\RouteMap;

class Router {

  protected $routes = array();

  protected $default_route_class = "\\Rakit\\Router\\Route";
  protected $default_routemap_class = "\\Rakit\\Router\\RouteMap";

  protected $route_class;
  protected $routemap_class;

  protected $group_paths = null;
  protected $curr_groups = array();

  protected $default_param_condition = '[a-zA-Z0-9_.-]+';
  protected $case_sensitive = true;

  public function __construct($route_class = null, $routemap_class = null)
  {
    $this->setRouteClass($route_class? $route_class : $this->default_route_class);
    $this->setRouteMapClass($routemap_class? $routemap_class : $this->default_routemap_class);
  }

  public function setRouteClass($route_class)
  { 
    $route_class = $this->rootClass($route_class);

    if( 
      is_subclass_of($route_class, $this->default_route_class) 
      OR $this->default_route_class == $route_class
      ) {
      $this->route_class = $route_class;
    } else {
      throw new \InvalidArgumentException("Route Class must be instanceof ".$this->default_route_class);
    }
  }

  public function setRouteMapClass($routemap_class)
  {
    $routemap_class = $this->rootClass($routemap_class);
    $default_routemap_class = $this->rootClass($this->default_routemap_class);

    if( 
      is_subclass_of($routemap_class, $default_routemap_class) 
      OR $default_routemap_class == $routemap_class
      ) {
      $this->routemap_class = $routemap_class;
    } else {
      throw new \InvalidArgumentException("RouteMap Class must be instanceof ".$default_routemap_class);
    }
  }

  public function caseSentitive($bool)
  {
    $this->case_sensitive = $bool;
  }

  protected function rootClass($class)
  {
    return "\\".ltrim($class,"\\");
  }

  public function getRouteClass()
  {
    return $this->rootClass($this->route_class);
  }

  public function getRouteMapClass()
  {
    return $this->rootClass($this->routemap_class);
  }

  public function getRoutes()
  {
    return $this->routes;
  }

  public function findRoutes($path_search)
  {
    $routes = array();
    $path_search = preg_replace("/[^a-zA-Z0-9]/", '\\\$0', $path_search);

    foreach($this->routes as $route) {
      if(preg_match("/^".$path_search."/", $route->getPath())) {
        $routes[] = $route;
      }
    }

    return $routes;
  }

  public function findRouteByName($name)
  {
    foreach($this->routes as $route) {
      if($route->getName() == $name) return $route;
    }

    return null;
  }

  public function register($methods, $path)
  {
    $route_class = $this->getRouteClass();

    if(! empty($this->group_paths)) {
      $prefix = implode("/", $this->group_paths);
      
      $path = preg_replace('/[\/]+/', '/', $prefix.$path);

      $route = new $route_class($methods, $path);

      foreach($this->curr_groups as $group) {
        $group->registerRoute($route);
      }
    } else {
      $route = new $route_class($methods, $path);
    }

    $this->routes[] = $route;
    
    return $route;
  }

  public function get($path)
  {
    return $this->register('GET', $path);
  }

  public function post($path)
  {
    return $this->register('POST', $path);
  }

  public function put($path)
  {
    return $this->register('PUT', $path);
  }

  public function patch($path)
  {
    return $this->register('PATCH', $path);
  }

  public function delete($path)
  {
    return $this->register('DELETE', $path);
  }

  public function map(array $routes_def)
  {
    $routes = array();
    $routemap_class = $this->getRouteMapClass();

    foreach($routes_def as $route_def) {
      if($route_def instanceof Route) {
        $routes[] = $route_def;
      } elseif($route_def instanceof RouteMap) {
        $routes = array_merge($routes, $route_def->getRoutes());
      } elseif(is_string($route_def)) {
        $route = $this->findRouteByName($route_def);
        if(!$route) throw new \Exception("Trying to map undeclared route named '{$route_def}'");

        $routes[] = $route;
      }
    }

    return new $routemap_class($routes);
  }

  public function group($path, Closure $grouper)
  {
    // router group mode...
    $this->group_paths[] = rtrim($path,"/");
    $route_map = $this->curr_groups[] = $this->map([]);

    // call grouper
    call_user_func($grouper, $this);

    // disable router group mode
    array_pop($this->group_paths);
    array_pop($this->curr_groups);

    return $route_map;
  }

  public function routeMatch($path, $method = null)
  {
    $path = '/'.ltrim($path, '/');

    foreach($this->routes as $route) {
      $regex = $this->routePathToRegex($route);
      $method_allowed = is_string($method)? in_array($method, $route->getAllowedMethods()) : true; 

      if(@preg_match($regex, $path, $match) AND $method_allowed) {
        $route_params = $this->getDeclaredPathParams($route);
        $route->params = array();

        foreach($route_params as $param) {
          if(isset($match[$param])) {
            $route->params[$param] = $match[$param];
          }
        }

        return $route;
      }
    }

    return null;
  }

  public function matchRequest()
  {
    $path = $_SERVER['PATH_INFO'];
    $method = $_SERVER['REQUEST_METHOD'];

    return $this->routeMatch($path, $method);
  }

  protected function routePathToRegex(Route $route)
  {
    $path = $route->getPath();
    $conditions = $route->getConditions();
    $params = $this->getDeclaredPathParams($route);

    $regex = $path;
    // transform /foo(/:bar(/:baz)) into foo(/:bar(/:baz)?)?
    $regex = preg_replace('/\)/i', '$0?', $regex);

    // transform /foo/:bar/:baz into /foo/(?<bar>)/(?<baz>)
    $regex = preg_replace('/:([a-zA-Z_][a-zA-Z0-9_]+)/', "(?<$1>)", $regex);

    // transform /foo/bar into \/foo\/bar
    $regex = str_replace('/', '\/', $regex);

    // transform /foo/(?<baz>)/(?<baz>) into /foo/(?<bar>{$condition})/(?<baz>{$condition})
    foreach($params as $param) {
      if(array_key_exists($param, $conditions)) {
        $regex = str_replace('(?<'.$param.'>)', '(?<'.$param.'>'.$conditions[$param].')', $regex);
      } else {
        $regex = str_replace('(?<'.$param.'>)', '(?<'.$param.'>'.$this->default_param_condition.')', $regex);
      }
    }

    return $this->case_sensitive? '/^'.$regex.'$/i' : '/^'.$regex.'$/';
  }

  protected function getDeclaredPathParams(Route $route)
  {
    $path = $route->getPath();
    preg_match_all('/\:([a-zA-Z_][a-zA-Z0-9_]+)/', $path, $match);
    return $match[1];
  }

}