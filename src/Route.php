<?php 

namespace Rakit\Router;

class Route {

  protected $path;

  protected $name;

  protected $allowed_methods = array();

  protected $conditions = array();

  protected $data = array();

  public function __construct($allowed_methods, $path)
  {
    if(is_string($allowed_methods)) $allowed_methods = [$allowed_methods];

    $this->allowed_methods = $allowed_methods;
    $this->path = $path;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getPath()
  {
    return $this->path;
  }

  public function getAllowedMethods()
  {
    return $this->allowed_methods;
  }

  public function getConditions()
  {
    return $this->conditions;
  }

  public function name($name)
  {
    $this->name = $name;

    return $this;
  }

  public function allowMethod($method)
  {
    $this->allowed_methods[] = $method;

    return $this;
  }  

  public function get($key, $default = NULL)
  {
    return isset($this->data[$key])? $this->data[$key] : $default;
  }

  public function set($key, $value)
  {
    $this->data[$key] = $value;

    return $this;
  }

  public function push($key, $value)
  {
    if( !isset($this->data[$key]) ) {
      $this->data[$key] = array();
    } elseif( !is_array($this->data[$key]) ) {
      $this->data[$key] = array($this->data[$key]);
    }

    $this->data[$key][] = $value;

    return $this;
  }

  public function where($param, $condition)
  {
    if(is_array($param)) {
      $this->conditions = $param;
      return;
    }
    $this->conditions[$param] = $condition;

    return $this;
  }

}
