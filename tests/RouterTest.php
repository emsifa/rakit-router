<?php

use Rakit\Router\Router;
use Rakit\Router\Route;
use Rakit\Router\RouteMap;

class RouterTest extends PHPUnit_Framework_TestCase {

    protected $router;

    public function setUp() {
        $this->router = new Router();
    }

    public function tearDown() {
        $this->router = null;
    }

    public function testRegisterRoute()
    {
        $routes = array(
            $this->router->register("GET", "/register"),
            $this->router->get("/get"),
            $this->router->post("/post"),
            $this->router->put("/put"),
            $this->router->patch("/patch"),
            $this->router->delete("/delete")
        );

        foreach($routes as $route) {
            $this->assertTrue($route instanceof Route);
        }
    }

    public function testRouteGroup()
    {
        $group_path = "/group";

        $route_group = $this->router->group($group_path, function($router) {

            $router->get("/get");
            $router->post("/post");
            $router->put("/put");
            $router->patch("/patch");
            $router->delete("/delete");

        })->set("foo", "bar");

        $routes = $this->router->getRoutes();
        $should_be = array(
            "/group/get" => ["GET"],
            "/group/post" => ["POST"],
            "/group/put" => ["PUT"],
            "/group/patch" => ["PATCH"],
            "/group/delete" => ["DELETE"],
        );

        $all_has_foo = true;
        foreach($routes as $route) {
            $this->assertTrue( 
                array_key_exists($route->getPath(), $should_be) 
                AND in_array($route->getAllowedMethods(), $should_be) 
            );

            if(!$route->get("foo")) $all_has_foo = false;
        }

        $this->assertTrue($all_has_foo);
    }

    public function testRouteMap()
    {
        $routes = array(
            'A' => $this->router->register("GET", "/register")->name("A"),
            'B' => $this->router->get("/get")->name("B"),
            'C' => $this->router->post("/post")->name("C"),
            'D' => $this->router->put("/put")->name("D"),
            'E' => $this->router->patch("/patch")->name("E"),
            'F' => $this->router->delete("/delete")->name("F")
        );

        $this->router->map(["A", "B", $routes["C"]])->set("foo", "bar");
        $has_foo = array("A", "B", "C");

        $correct = true;

        foreach($routes as $route) {
            $name = $route->getName();
            if( in_array($name, $has_foo) ) {
                if( "bar" !== $route->get("foo") ) $correct = false;
            } else {
                if( $route->get("foo") ) $correct = false;
            }
        }

        $this->assertTrue($correct);
    }

    public function testMatching()
    {
        // simple route
        $this->router->get("/simple");
        $this->assertTrue( $this->router->routeMatch("/simple") instanceof Route );
        $this->assertTrue( $this->router->routeMatch("/simple", "GET") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/simple", "POST") instanceof Route );

        // route param
        $this->router->get("/param/:param");
        $this->assertTrue( $this->router->routeMatch("/param/foo") instanceof Route );
        $this->assertTrue( $this->router->routeMatch("/param/foo", "GET") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/param/foo", "POST") instanceof Route );

        // route multiple param
        $this->router->get("/multi/:p1/:p2");
        $this->assertTrue( $this->router->routeMatch("/multi/foo/bar") instanceof Route );
        $this->assertTrue( $this->router->routeMatch("/multi/foo/bar", "GET") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/multi/foo/bar", "POST") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/multi/foo") instanceof Route );

        // route optional param
        $this->router->get("/opt/:p1(/:p2)");
        $this->assertTrue( $this->router->routeMatch("/opt/foo/bar") instanceof Route );
        $this->assertTrue( $this->router->routeMatch("/opt/foo/bar", "GET") instanceof Route );
        $this->assertTrue( $this->router->routeMatch("/opt/foo") instanceof Route );

        // route custom conditional param
        $this->router->get("/cond/:param")->where("param", "[0-9]+");        
        $this->assertTrue( $this->router->routeMatch("/cond/123") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/cond/abc") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/cond/a12") instanceof Route );
        $this->assertFalse( $this->router->routeMatch("/cond/12c") instanceof Route );

    }

}